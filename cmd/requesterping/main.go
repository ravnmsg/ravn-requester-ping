package main

import (
	"context"
	"log"
	"os"

	"gitlab.com/ravnmsg/ravn-requester/pkg/app"
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server/httpserver"
)

func main() {
	srv, err := httpserver.New(os.Getenv("RAVN_REQUESTER_PING_SERVER_PORT"))
	if err != nil {
		log.Printf("Error creating HTTP server: %s", err)
		os.Exit(1)
	}

	if err := app.Start(context.Background(), srv, pingRequestFn); err != nil {
		log.Printf("Error configuring application: %s", err)
		os.Exit(1)
	}
}

// pingRequestFn ignores any input *Values and returns a simple hard-coded output.
func pingRequestFn(_ *requester.Values) (*requester.Values, error) {
	output := requester.Values{
		"externalId": "ping",
	}

	return &output, nil
}
