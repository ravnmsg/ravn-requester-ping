# Contributing

The Ravn application is written in [Go](https://golang.org/). All contributions must pass through a GitLab issue and a merge request at the [Ravn Ping Requester repository](https://gitlab.com/ravnmsg/ravn-requester-ping).

This project is released with a Contributor Code of Conduct. Participation in this project implies agreement to abide by its terms.

## Issues

Most changes to the Ravn Ping Requester service must go through [issues](https://gitlab.com/ravnmsg/ravn-requester-ping/-/issues). This is where the community can discuss a specific problem or proposal.

## Merge requests

A [merge request](https://gitlab.com/ravnmsg/ravn-requester-ping/-/merge_requests) to the `master` branch must be created. This is also where the community will discuss the code related to the current issue.

## Continuous integration

A continuous integration workflow is setup to automatically run tests on each commit. Merge requests will not be accepted if a test is failing.

## Local development

It is recommended to install [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) when working on the service.

### Installation

From a terminal and in the service folder, the following commands will build the Docker image and start the Ping Requester service:

```bash
docker-compose build
docker-compose up --detach
```

The service can be left running during development, and can be removed later with the following command:

```bash
docker-compose down
```

### Development

The Ping Requester service can be tested via the terminal:

```bash
$ curl -XPOST localhost:50100/request --data '{"key":"value"}' | jq
{
  "externalId": "ping"
}
```

The result shown above is the designed behavior of the Ping Requester.

After modifying the code, the Docker image needs to be rebuilt. The following command will rebuild the image and, if already running, restart the service automatically:

```bash
make build
```

### Tests

The Ravn codebase must pass all style checks and tests. The following commands can be used locally:

```bash
# Simple command to execute tests
go test ./...

# Runs all tests
make test

# Runs style checks
make test-lint
```
