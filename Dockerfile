# golang:1.14-alpine3.11
FROM golang@sha256:3116782b7d4e7ad51454bbb0ab1291468b7cb23e2286f6fcce50d6163289b6dd as builder
WORKDIR /app

COPY go.mod go.sum /app/
RUN go mod download

ENV CGO_ENABLED=0
COPY . /app/
RUN go build -o /go/bin/requesterping ./cmd/requesterping

# The following builds from scratch an image with only the binary file.
FROM scratch
LABEL maintainer="sbernier@ravnmsg.io"
WORKDIR /app

COPY --from=builder /go/bin/requesterping /app/

USER 10001:10001

ENTRYPOINT ["./requesterping"]
