# Ravn Ping Requester

The Ping Requester service is an internal service used for tests and monitoring. It doesn't do anything with the message, except by responding with a hard-coded `{"externalId":"ping"}` body.

## Usage

The service's latest Docker image can be pulled from `registry.gitlab.com/ravnmsg/ravn-requester-ping:latest`. The following command will retrieve and start the service on port `50100`:

```bash
docker run --publish 50100:50100 --env RAVN_REQUESTER_PING_SERVER_PORT=50100 --rm registry.gitlab.com/ravnmsg/ravn-requester-ping:latest
```

The service will then respond to requests on the `/request` path.

```bash
$ curl -XPOST localhost:50100/request | jq
{
    "externalId": "ping"
}
```

This service will need to be configured in the database. Given a Ravn application listening on `localhost:50000`, the following command will create a new delivery kind resource that will represent the Ping Requester service:

```bash
curl -XPOST localhost:50000/deliveryKinds --data '{"code":"ping","host":"localhost:50100"}'
```

Of course, the `localhost` must be changed to the correct service name if the Requester service is handled through Kubernetes or Docker Compose.
